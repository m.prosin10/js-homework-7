// Завдання 1

const product = {
  name: "T-Shirt",
  price: 120,
  discount: 35,
  getPriceWithDiscount: function () {
    return this.price - this.price * (this.discount / 120);
  },
};

console.log(
  "Повна ціна з урахуванням знижки: " + product.getPriceWithDiscount()
);

// Завдання 2

function greeting(user) {
  return `Привіт,мені ${user.age} років`;
}

const userName = prompt("Введіть своє ім'я:");
const userAge = prompt("Введіть свій вік:");

const user = {
  name: userName,
  age: userAge,
};

alert(greeting(user));

// Завдання 3

function deepClone(obj) {
  if (obj === null || typeof obj !== "object") {
    return obj;
  }

  const clone = Array.isArray(obj) ? [] : {};

  for (const key in obj) {
    if (obj.hasOwnProperty(key)) {
      clone[key] = deepClone(obj[key]);
    }
  }

  return clone;
}

const original = {
  name: "Original",
  nested: {
    value: 42,
    array: [1, 2, 3],
  },
};

const cloned = deepClone(original);

console.log("Оригінал:", original);
console.log("Клон:", cloned);

original.nested.value = 100;
console.log("Змінений оригінал:", original);
console.log("Клон після зміни оригіналу:", cloned);
